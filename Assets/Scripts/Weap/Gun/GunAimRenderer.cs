using System;
using System.Collections;
using UnityEngine;

public class GunAimRenderer : MonoBehaviour
{
    public static GunAimRenderer CreateInstance()
        => Instantiate(Resources.Load<GunAimRenderer>("Weapon/AimRenderer"));

    private static bool setATeamType = false;

    private LineRenderer m_lineRenderer;
    private LineRenderer lineRenderer => m_lineRenderer ?? (m_lineRenderer = transform.GetComponent<LineRenderer>());

    private Transform m_ShotPos;
    private Func<Vector3> m_ShotDir;
    private float length = 2;

    public bool IsValid()
    {
        return m_ShotPos != null && m_ShotDir != null;
    }

    public void Initialize(Gun gun, Transform shotPos, Func<Vector3> shotDir)
    {
        m_ShotPos = shotPos;
        m_ShotDir = shotDir;

        lineRenderer.positionCount = 2;
        lineRenderer.SetWidth(.02f, .02f);

        UnitDefaultStatus defaultStatus = Unit.unitDefaultStatus;
        Color[] targetColor = null;
        if (setATeamType == false)
        {
            targetColor = new Color[] { defaultStatus.ATeamAimRendererColors[0], defaultStatus.ATeamAimRendererColors[1] };
            setATeamType = true;
        }
        else
        {
            targetColor = new Color[] { defaultStatus.BTeamAimRendererColors[0], defaultStatus.BTeamAimRendererColors[1] };
        }

        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(targetColor[0], 0.0f), new GradientColorKey(targetColor[1], 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) }
        );

        lineRenderer.colorGradient = gradient;

        transform.SetParent(gun.transform);
    }

    private IEnumerator SetColor(Gun gun)
    {
        while(true)
        {
            yield return null;
            Debug.Log("Test");

            if (gun.user != null) break;
        }
        UnitDefaultStatus status = Unit.unitDefaultStatus;
        Color[] colors = gun.user.teamType == Team.TeamType.A ? status.ATeamAimRendererColors : status.BTeamAimRendererColors;

        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(colors[0], 0.0f), new GradientColorKey(colors[1], 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) }
        );

        lineRenderer.colorGradient = gradient;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsValid() == false) return;

        Vector3 targetPos = m_ShotPos.position + length * m_ShotDir();

        lineRenderer.SetPosition(0, m_ShotPos.position);
        lineRenderer.SetPosition(1, targetPos);
    }
}
