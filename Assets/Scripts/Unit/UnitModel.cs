using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitModel : MonoBehaviour
{
    public Transform[] HandlePos_Left;
    public Transform[] HandlePos_Right;

    public Transform modelPos;

    public Transform head;
    public Transform body;
    public Transform backPack;
}
